const express = require('express');
const app = express();
const cors = require('cors');
const port = 8008;
const mysqlDb = require('./mysqlDb');
const news = require('./app/news');
const comments = require('./app/comments');

app.use(cors());
app.use(express.json());
app.use('/news', news);
app.use('/comments', comments);
app.use(express.static('public'));

mysqlDb.connect().catch(e => console.log(e));
app.listen(port, () => {
    console.log(`Server create on ${port} port!`);
});