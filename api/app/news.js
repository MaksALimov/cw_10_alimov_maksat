const express = require('express');
const router = express.Router();
const mysqlDb = require('../mysqlDb');
const config = require('../config');
const {nanoid} = require('nanoid');
const path = require('path');
const multer = require('multer');

const storage = multer.diskStorage({
    destination: (req, file, cb) => {
        cb(null, config.uploadPath);
    },

    filename: (req, file, cb) => {
        cb(null, nanoid() + path.extname(file.originalname));
    },
});

const upload = multer({storage});


router.get('/', async (req, res) => {
    const [news] = await mysqlDb.getConnection().query(`SELECT id, title, image, datetime FROM ??`, ['news']);
    res.send(news);
});

router.get('/:id', async (req, res) => {
    const [oneNews] = await mysqlDb.getConnection().query(`SELECT * FROM ?? where id = ?`,
        ['news', req.params.id]
    );

    res.send(oneNews[0]);
});

router.post('/', upload.single('image'), async (req, res) => {
    if (!req.body.title || !req.body.content) {
        return res.status(401).send({error: 'title and content must be filled'});
    }

    const newPostData = {
        title: req.body.title,
        content: req.body.content,
    };

    if (req.body.file) {
        console.log(req.body.file);
        newPostData.image = req.file.filename;
    }

    await mysqlDb.getConnection().query(
        `INSERT INTO ?? (title, content, image) values (?, ?, ?)`,
        ['news', newPostData.title, newPostData.content, newPostData.image]
    );

    res.send({message: 'Your post has been successfully added!'});
});

router.delete('/:id', async (req, res) => {
    await mysqlDb.getConnection().query(`DELETE FROM ?? where id = ?`,
        ['news', req.params.id]
    );

    res.send({message: 'Your post has been successfully deleted!'});
});

module.exports = router;