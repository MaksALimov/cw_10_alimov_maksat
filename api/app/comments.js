const express = require('express');
const mysqlDb = require('../mysqlDb');
const router = express.Router();

router.get('/', async (req, res) => {
    const [comments] = await mysqlDb.getConnection().query(`SELECT * FROM ??`, ['comments']);

    if (req.query.news_id) {
        const [comments] = await mysqlDb.getConnection().query(`SELECT * FROM ?? where news_id = ?`,
            ['comments', req.query.news_id]
        );
        return res.send(comments);
    }

    res.send(comments);
});

router.post('/', async (req, res) => {
    const newCommentData = {
        author: req.body.author || 'Anonymous',
        comment: req.body.comment,
        id: req.body.id
    };

    //можно добавить комментарий если он связан с новостью, в любом другом случае - ошибка
    try {
        await mysqlDb.getConnection().query(`INSERT INTO ?? (news_id, author, comment) values(?, ?, ?)`,
            ['comments', newCommentData.id, newCommentData.author, newCommentData.comment]
        );
    } catch (e) {
        return res.status(401).send({error: 'Comment cannot be added'});
    }

    return res.send({message: 'Your comment has been added!'});
});

router.delete('/:id', async (req, res) => {
    await mysqlDb.getConnection().query(`DELETE FROM ?? where id = ?`,
        ['comments', req.params.id]
    );

    return res.send({message: 'Your comment has been deleted!'});
});

module.exports = router;