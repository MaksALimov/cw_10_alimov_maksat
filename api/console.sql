create database if not exists news;

use news;

create table if not exists news (
    id int not null auto_increment primary key,
    title varchar(255) not null,
    content text not null,
    image varchar(255) null,
    datetime timestamp default NOW()
);

create table if not exists comments (
    id int not null auto_increment primary key,
    news_id int not null,
    author varchar(255) null,
    comment text not null,
    constraint comments_news_id_fk
    foreign key (news_id)
    references news(id)
    on delete cascade
    on update cascade
);

insert into news (title, content, image)
values ('Some title', 'Some content', null);

insert into comments (news_id, author, comment)
values (24, 'John Doe', 'What a good day!');