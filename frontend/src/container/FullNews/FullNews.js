import React, {useState} from 'react';
import {useDispatch, useSelector} from "react-redux";
import {Button, Grid, makeStyles, TextField, Typography} from "@material-ui/core";
import dayjs from "dayjs";
import {addComment} from "../../store/actions/getCommentsActions";

const useStyles = makeStyles(() => ({
    content: {
        margin: '10px 0',
        letterSpacing: '1px',
    },

    comments: {
        boxShadow: 'rgba(6, 24, 44, 0.4) 0px 0px 0px 2px, rgba(6, 24, 44, 0.65) 0px 4px 6px -1px, rgba(255, 255, 255, 0.08) 0px 1px 0px inset',
        padding: '20px',
        margin: '20px 0'
    },

    addCommentTitle: {
        margin: '20px 0',
    },

    textFields: {
        margin: '20px 0',
    },

}));

const FullNews = ({match}) => {
    const classes = useStyles();
    const dispatch = useDispatch();
    const fullNews = useSelector(state => state.allPosts.post);
    const comments = useSelector(state => state.getComments.comments);

    const [comment, setComment] = useState({
        author: '',
        comment: '',
    });

    const onSubmitForm = e => {
        e.preventDefault();
        dispatch(addComment(comment, match.params.id));
    };


    const onInputChangeHandler = e => {
        const {name, value} = e.target;
        setComment(prevState => ({...prevState, [name]: value}));
    };

    return fullNews && (
        <Grid container direction="column">
            <Typography variant="h4">{fullNews.title}</Typography>
            <Typography className={classes.content}>
                At: {dayjs(new Date(fullNews.datetime)).format('YYYY-MM-DD HH:mm')}
            </Typography>
            <Typography className={classes.content} variant="h5">{fullNews.content}</Typography>

            <Typography variant="h3">Comments</Typography>
            {comments.map(comment => (
                <Grid item container direction="column" key={comment.id} className={classes.comments}>
                    <Typography variant="h4">
                        {comment.author} wrote: {comment.comment}
                    </Typography>
                </Grid>
            ))}

            <form onSubmit={onSubmitForm}>
                <Typography variant="h4" className={classes.addCommentTitle}>Add Comment</Typography>
                <Grid container direction="column">
                    <TextField
                        className={classes.textFields}
                        id="outlined-basic"
                        label="Name"
                        name="author"
                        value={comment.author}
                        onChange={onInputChangeHandler}
                        variant="outlined"
                    />
                    <TextField
                        className={classes.textFields}
                        id="outlined-basic"
                        label="Comment"
                        name="comment"
                        value={comment.comment}
                        onChange={onInputChangeHandler}
                        variant="outlined"
                    />

                    <Grid container>
                        <Button
                            variant="outlined"
                            type="submit"
                        >
                            Add
                        </Button>
                    </Grid>
                </Grid>
            </form>
        </Grid>
    );
};

export default FullNews;