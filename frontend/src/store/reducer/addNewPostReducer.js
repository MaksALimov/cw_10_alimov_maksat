import {ADD_NEW_POST_FAILURE, ADD_NEW_POST_REQUEST, ADD_NEW_POST_SUCCESS} from "../actions/addNewPostActions";

const initialState = {
    error: null,
    loading: null,
};

const addNewPostReducer = (state = initialState, action) => {
    switch (action.type) {
        case ADD_NEW_POST_REQUEST:
            return {...state, loading: true};

        case ADD_NEW_POST_SUCCESS:
            return {...state, loading: false};

        case ADD_NEW_POST_FAILURE:
            return {...state, error: action.payload, loading: false};

        default:
            return state;
    }
};

export default addNewPostReducer;