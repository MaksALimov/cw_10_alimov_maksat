import {FETCH_COMMENTS_FAILURE, FETCH_COMMENTS_REQUEST, FETCH_COMMENTS_SUCCESS} from "../actions/getCommentsActions";

const initialState = {
    error: null,
    loading: null,
    comments: [],
};

const getCommentsReducer = (state = initialState, action) => {
    switch (action.type) {
        case FETCH_COMMENTS_REQUEST:
            return {...state, loading: true};

        case FETCH_COMMENTS_SUCCESS:
            return {...state, comments: action.payload, error: false};

        case FETCH_COMMENTS_FAILURE:
            return {...state, error: action.payload, loading: false};

        default:
            return state;
    }
};

export default getCommentsReducer;