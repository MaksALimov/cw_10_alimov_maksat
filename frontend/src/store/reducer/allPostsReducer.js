import {
    FETCH_ALL_POSTS_FAILURE,
    FETCH_ALL_POSTS_REQUEST,
    FETCH_ALL_POSTS_SUCCESS, FETCH_ONE_NEWS_FAILURE,
    FETCH_ONE_NEWS_REQUEST, FETCH_ONE_NEWS_SUCCESS
} from "../actions/allPostsActions";

const initialState = {
    loading: null,
    error: false,
    posts: [],
    post: null,
};

const allPostsReducer = (state = initialState, action) => {
    switch (action.type) {
        //группировка case
        case FETCH_ALL_POSTS_REQUEST:
        case FETCH_ONE_NEWS_REQUEST:
            return {...state, error: null, loading: true}

        case FETCH_ALL_POSTS_SUCCESS:
            return {...state, posts: action.payload, error: false};

        case FETCH_ALL_POSTS_FAILURE:
        case FETCH_ONE_NEWS_FAILURE:
            return {...state, error: action.payload, loading: false};

        case FETCH_ONE_NEWS_SUCCESS:
           return {...state, error: action.payload, loading: false, post: action.payload}

        default:
            return state
    }
}

export default allPostsReducer;