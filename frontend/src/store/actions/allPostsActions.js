import axios from "axios";

export const FETCH_ALL_POSTS_REQUEST = 'FETCH_ALL_POSTS_REQUEST';
export const FETCH_ALL_POSTS_SUCCESS = 'FETCH_ALL_POSTS_SUCCESS';
export const FETCH_ALL_POSTS_FAILURE = 'FETCH_ALL_POSTS_FAILURE';

export const FETCH_ONE_NEWS_REQUEST = 'FETCH_ONE_NEWS_REQUEST';
export const FETCH_ONE_NEWS_SUCCESS = 'FETCH_ONE_NEWS_SUCCESS';
export const FETCH_ONE_NEWS_FAILURE = 'FETCH_ONE_NEWS_FAILURE';

export const FETCH_DELETE_REQUEST = 'FETCH_DELETE_REQUEST';
export const FETCH_DELETE_SUCCESS = 'FETCH_DELETE_SUCCESS';
export const FETCH_DELETE_FAILURE = 'FETCH_DELETE_FAILURE';

export const fetchAllPostsRequest = () => ({type: FETCH_ALL_POSTS_REQUEST});
export const fetchAllPostsSuccess = allPosts => ({type: FETCH_ALL_POSTS_SUCCESS, payload: allPosts});
export const fetchAllPostsFailure = error => ({type: FETCH_ALL_POSTS_FAILURE, payload: error});

export const fetchOneNewsRequest = () => ({type: FETCH_ONE_NEWS_REQUEST});
export const fetchOneNewsSuccess = news => ({type: FETCH_ONE_NEWS_SUCCESS, payload: news});
export const fetchOneNewsFailure = error => ({type: FETCH_ONE_NEWS_FAILURE, payload: error});

export const fetchDeleteRequest = () => ({type: FETCH_DELETE_REQUEST});
export const fetchDeleteSuccess = () => ({type: FETCH_DELETE_SUCCESS});
export const fetchDeleteFailure = error => ({type: FETCH_DELETE_FAILURE, payload: error});

export const getAllPosts = () => {
    return async dispatch => {
        try {
            dispatch(fetchAllPostsRequest());
            const response = await axios.get('http://127.0.0.1:8008/news');
            dispatch(fetchAllPostsSuccess(response.data));
        } catch (error) {
            dispatch(fetchAllPostsFailure(error));
        }
    };
};

export const getOnePost = newsId => {
    return async dispatch => {
        try {
            dispatch(fetchOneNewsRequest());
            const response = await axios.get(`http://127.0.0.1:8008/news/${newsId}`);
            dispatch(fetchOneNewsSuccess(response.data));

        } catch (error) {
            dispatch(fetchOneNewsFailure(error));
        }
    };
};

export const deletePost = id => {
    return async dispatch => {
      try {
          dispatch(fetchDeleteRequest());
          await axios.delete(`http://127.0.0.1:8008/news/${id}`);
      }  catch (error) {
          dispatch(fetchDeleteFailure(error));
      }
    };
};