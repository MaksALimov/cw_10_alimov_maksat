import axios from "axios";

export const ADD_NEW_POST_REQUEST = 'ADD_NEW_POST_REQUEST';
export const ADD_NEW_POST_SUCCESS = 'ADD_NEW_POST_SUCCESS';
export const ADD_NEW_POST_FAILURE = 'ADD_NEW_POST_FAILURE';

export const addNewPostRequest = () => ({type: ADD_NEW_POST_REQUEST});
export const addNewPostSuccess = () => ({type: ADD_NEW_POST_SUCCESS});
export const addNewPostFailure = error => ({type: ADD_NEW_POST_FAILURE});

export const addNewPost = post => {
    return async dispatch => {
        try {
            dispatch(addNewPostRequest());
            await axios.post('http://127.0.0.1:8008/news', post);
        } catch (error) {
            dispatch(addNewPostFailure(error));
        }
    };
};