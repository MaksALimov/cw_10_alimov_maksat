import axios from "axios";

export const FETCH_COMMENTS_REQUEST = 'FETCH_COMMENTS_REQUEST';
export const FETCH_COMMENTS_SUCCESS = 'FETCH_COMMENTS_SUCCESS';
export const FETCH_COMMENTS_FAILURE = 'FETCH_COMMENTS_FAILURE';

export const ADD_COMMENT_REQUEST = 'ADD_COMMENT_REQUEST';
export const ADD_COMMENT_SUCCESS = 'ADD_COMMENT_SUCCESS';
export const ADD_COMMENT_FAILURE = 'ADD_COMMENT_FAILURE';

export const fetchCommentsRequest = () => ({type: FETCH_COMMENTS_REQUEST});
export const fetchCommentsSuccess = comments => ({type: FETCH_COMMENTS_SUCCESS, payload: comments});
export const fetchCommentsFailure = error => ({type: FETCH_COMMENTS_FAILURE, payload: error});

export const addCommentRequest = () => ({type: ADD_COMMENT_REQUEST});
export const addCommentSuccess = () => ({type: ADD_COMMENT_SUCCESS});
export const addCommentFailure = error => ({type: ADD_COMMENT_FAILURE, payload: error});

export const getComments = id => {
    return async dispatch => {
        try {
            dispatch(fetchCommentsRequest());
            const response = await axios.get(`http://127.0.0.1:8008/comments/?news_id=${id}`);

            dispatch(fetchCommentsSuccess(response.data));
        } catch (error) {
            dispatch(fetchCommentsFailure(error));
        }
    };
};

export const addComment = (comment, id) => {
    return async dispatch => {
        try {
            dispatch(addCommentRequest());
            comment.id = id
            await axios.post(`http://127.0.0.1:8008/comments`, comment);
        } catch (error) {
            dispatch(addCommentFailure(error));
        }
    }
};

