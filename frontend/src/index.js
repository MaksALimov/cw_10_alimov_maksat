import React from 'react';
import ReactDOM from 'react-dom';
import App from './App';
import {applyMiddleware, combineReducers, compose, createStore} from "redux";
import thunk from "redux-thunk";
import {Provider} from "react-redux";
import allPostsReducer from "./store/reducer/allPostsReducer";
import {BrowserRouter} from "react-router-dom";
import {CssBaseline} from "@material-ui/core";
import getCommentsReducer from "./store/reducer/getCommentsReducer";
import addNewPostReducer from "./store/reducer/addNewPostReducer";

const rootReducer = combineReducers({
    allPosts: allPostsReducer,
    getComments: getCommentsReducer,
    addNewPost: addNewPostReducer,
});

const composeEnhancers = window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__ || compose;

const store = createStore(rootReducer, composeEnhancers(
    applyMiddleware(thunk)
));

const app = (
    <Provider store={store}>
        <CssBaseline/>
        <BrowserRouter>
            <App/>
        </BrowserRouter>
    </Provider>
);

ReactDOM.render(app, document.getElementById('root'));