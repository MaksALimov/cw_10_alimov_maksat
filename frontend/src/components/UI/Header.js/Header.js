import React from 'react';
import {Button, Grid, makeStyles, Typography} from "@material-ui/core";
import {Link} from "react-router-dom";

const useStyles = makeStyles(() => ({
    title: {
        marginTop: '30px',
    },

    posts: {
        margin: '40px 0',
    }
}))

const Header = () => {
    const classes = useStyles();
    return (
        <Grid item>
            <Grid container direction="row" justifyContent="space-between">
                <Typography variant="h4" className={classes.posts}>Posts</Typography>
                <Button
                    variant="contained"
                    className={classes.posts}
                >
                    <Link to="/new-post">
                        Add New Post
                    </Link>
                </Button>
            </Grid>
        </Grid>
    );
};

export default Header;