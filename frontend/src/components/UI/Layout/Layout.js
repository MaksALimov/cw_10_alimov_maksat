import React from 'react';
import Header from "../Header.js/Header";
import {Container} from "@material-ui/core";

const Layout = ({children}) => {
    return (
        <Container>
            <Header/>
            {children}
        </Container>
    );
};

export default Layout;