import React, {useEffect} from 'react';
import {useDispatch, useSelector} from "react-redux";
import {getAllPosts} from "../../store/actions/allPostsActions";
import {Grid, Typography} from "@material-ui/core";
import News from "../News/News";


const AllPosts = () => {
    const dispatch = useDispatch();
    const allPosts = useSelector(state => state.allPosts.posts);
    useEffect(() => {
        dispatch(getAllPosts());
    }, [dispatch]);

    console.log(allPosts);

    return (
        <>
            <Grid container>
                <Typography variant="h3">News</Typography>
                {allPosts.map(news => (
                   <News
                       key={news.id}
                       title={news.title}
                       datetime={news.datetime}
                       image={news.image}
                       id={news.id}
                   />
                ))}
            </Grid>
        </>
    );
};

export default AllPosts;