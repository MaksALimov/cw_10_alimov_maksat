import React, {useState} from 'react';
import {Button, Grid, makeStyles, TextField, Typography} from "@material-ui/core";
import {useDispatch} from "react-redux";
import {addNewPost} from "../../store/actions/addNewPostActions";

const useStyles = makeStyles(() => ({
    addPost: {
        margin: '20px 0'
    },
    textFields: {
        margin: '20px 0'
    },
}));

const AddNewPost = () => {
    const classes = useStyles();
    const dispatch = useDispatch();

    const [post, setPost] = useState({
        title: '',
        content: '',
        image: null,
    });

    const onSubmitForm = e => {
        e.preventDefault();

        const formData = new FormData();
        Object.keys(post).forEach(key => {
            formData.append(key, post[key]);
        });

        dispatch(addNewPost(formData));
    };

    const onInputChangeHandler = e => {
      const {name, value} = e.target;

      setPost(prevState => ({...prevState, [name] : value}));
    };

    const fileChangeHandler = e => {
        const name = e.target.name;
        const file = e.target.files[0];
        setPost(prevState => {
            return {...prevState, [name]: file}
        });
    };

    return (
        <>
            <Typography
                className={classes.addPost}
                variant="h4">
                Add new Post</Typography>
            <form onSubmit={onSubmitForm}>
                <Grid container direction="column" alignItems="center">
                   <Grid item className={classes.textFields}>
                       <TextField
                           label="Title"
                           name="title"
                           value={post.title}
                           onChange={onInputChangeHandler}
                           variant="outlined" />
                   </Grid>
                    <Grid item className={classes.textFields}>
                        <TextField
                            label="Content"
                            name="content"
                            value={post.content}
                            onChange={onInputChangeHandler}
                            variant="outlined"
                            multiline
                        />
                    </Grid>
                    <Grid item className={classes.textFields}>
                        <TextField
                            type="file"
                            name="image"
                            onChange={fileChangeHandler}
                        />
                    </Grid>
                    <Grid item className={classes.textFields}>
                        <Button
                            variant="outlined"
                            type="submit"
                        >
                            Save
                        </Button>
                    </Grid>
                </Grid>
            </form>
        </>
    );
};

export default AddNewPost;