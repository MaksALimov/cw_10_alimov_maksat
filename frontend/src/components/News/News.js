import React from 'react';
import {Button, Grid, makeStyles, Typography} from "@material-ui/core";
import {Link} from "react-router-dom";
import {useDispatch} from "react-redux";
import {deletePost, getOnePost} from "../../store/actions/allPostsActions";
import {getComments} from "../../store/actions/getCommentsActions";

const useStyles = makeStyles(() => ({
    container: {
        borderRadius: '10px',
        margin: '10px 0',
        padding: '20px',
        boxShadow: 'rgba(14, 30, 37, 0.12) 0px 2px 4px 0px, rgba(14, 30, 37, 0.32) 0px 2px 16px 0px',
    },

    buttons: {
        margin: '10px 0',
    }
}));

const News = ({title, datetime, image, id}) => {
    const classes = useStyles();
    const dispatch = useDispatch();

    const getNews = () => {
        dispatch(getOnePost(id));
        dispatch(getComments(id));
    }

    return (
        <Grid item container direction="column" className={classes.container}>
            <Typography variant="h5">{title}</Typography>
            <Typography>{datetime}</Typography>
            <Grid item container direction="row" justifyContent="space-between">
                <Button
                    onClick={getNews}
                    variant="contained"
                    className={classes.buttons}>
                    <Link to={`/news/${id}`}>
                        Read Full Post
                    </Link>
                </Button>
                <Button
                    onClick={() => dispatch(deletePost(id))}
                    variant="contained"
                    className={classes.buttons}>
                    Delete
                </Button>
            </Grid>
        </Grid>
    );
};

export default News;