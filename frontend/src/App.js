import AllPosts from "./components/AllPosts/AllPosts";
import Layout from "./components/UI/Layout/Layout";
import {Route, Switch} from "react-router-dom";
import FullNews from "./container/FullNews/FullNews";
import AddNewPost from "./components/AddNewPost/AddNewPost";

const App = () => (
    <Layout>
        <Switch>
            <Route path="/" exact component={AllPosts}/>
            <Route path="/news/:id" component={FullNews}/>
            <Route path="/new-post" component={AddNewPost}/>
        </Switch>
    </Layout>
);

export default App;
